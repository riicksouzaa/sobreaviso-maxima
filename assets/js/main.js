/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


// var form = $('#form-salario');

window.onload = function () {
    $.ajax({
        url: "Ajax.php",
        type: "POST",
        data: {type:'sobreaviso'},
        dataType: 'json',
        beforeSend: function () {
            $("#j-loader").addClass("lds-roller");
        },
        success: function (sobreaviso) {
            $("#j-loader").removeClass("lds-roller");
            if (sobreaviso.status !== 'danger') {
                $("#j-sobreaviso").html('Seu salário: R$' + sobreaviso.salario + '<br>'
                    + 'Valor da sua Hora trabalhada: R$' + sobreaviso.horatrabalhada + '<br>'
                    + 'Valor da Hora do Sobreaviso: R$' + sobreaviso.horasobreaviso + '<br>'
                    + 'Valor de 12H de Sobreaviso: R$' + sobreaviso.middaysobreaviso + '<br>'
                    + 'Valor de 24H de Sobreaviso: R$' + sobreaviso.alldaysobreaviso).addClass('text-maxima-blue');
            } else {
                $("#j-sobreaviso").html(sobreaviso.msg);
            }
        }

    });
};

// form.submit(function () {
//     var data = form.serialize();
//     var url = form.attr('action');
//     var type = form.attr('method');
//     $.ajax({
//         url: url,
//         type: type,
//         data: data,
//         dataType: 'json',
//         beforeSend: function () {
//             $("#j-loader").addClass("lds-roller");
//         },
//         success: function (sobreaviso) {
//             $("#j-loader").removeClass("lds-roller");
//             if (sobreaviso.status !== 'danger') {
//                 $("#j-sobreaviso").html('Seu salário: R$' + sobreaviso.salario + '<br>'
//                     + 'Valor da sua Hora trabalhada: R$' + sobreaviso.horatrabalhada + '<br>'
//                     + 'Valor da Hora do Sobreaviso: R$' + sobreaviso.horasobreaviso + '<br>'
//                     + 'Valor de 12H de Sobreaviso: R$' + sobreaviso.middaysobreaviso + '<br>'
//                     + 'Valor de 24H de Sobreaviso: R$' + sobreaviso.alldaysobreaviso).addClass('text-maxima-blue');
//             } else {
//                 $("#j-sobreaviso").html(sobreaviso.msg);
//             }
//         }
//
//     });
//     return false;
// });