$(document).ready(function () {
    $("#nome").focus();
});

$('#cadastro').submit(function (e) {
    e.preventDefault();
}).validate({
    rules: {
        'terms': {
            required: true
        },
        'nome': {
            minlength: 4
        },
        'username': {
            minlength: 4
        },
        'confirm': {
            equalTo: '[name="password"]'
        }
    },
    highlight: function (input) {
        $(input).parents('.form-line').addClass('error');
    },
    unhighlight: function (input) {
        $(input).parents('.form-line').removeClass('error');
        $(input).parents('.form-line').addClass('focused');
    },
    errorPlacement: function (error, element) {
        $(element).parents('.input-group').append(error);
        $(element).parents('.form-group').append(error);
    },
    submitHandler: function (a) {
        console.log(a);
        let form = $('#cadastro');
        let data = form.serialize();
        let url = form.attr('action');
        let type = form.attr('method');

        $.ajax({
            url: url,
            data: data,
            type: type,
            dataType: 'JSON',
            beforeSend: function () {
                $("#j-loader").addClass("lds-roller");
            },
            success: function (cadastro) {
                if (cadastro.status !== 'error') {
                    $(location).attr('href', './');
                    $("#j-loader").removeClass("lds-roller");
                } else {
                    $(".j-error").appendTo('<p>' + cadastro.msg + '</p>');
                    $("#j-loader").removeClass("lds-roller");
                }
            }
        });
        return false;
    }
});
