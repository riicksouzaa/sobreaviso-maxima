$(document).ready(function () {
    $("#username").focus();
});

$('#login').submit(function (e) {
    e.preventDefault();
}).validate({
    rules: {
        'terms': {
            required: true
        },
        'username': {
            minlength: 4
        }
    },
    highlight: function (input) {
        $(input).parents('.form-line').addClass('error');
    },
    unhighlight: function (input) {
        $(input).parents('.form-line').removeClass('error');
        $(input).parents('.form-line').addClass('focused');
    },
    errorPlacement: function (error, element) {
        $(element).parents('.input-group').append(error);
        $(element).parents('.form-group').append(error);
    },
    submitHandler: function () {
        let form = $('#login');
        let data = form.serialize();
        let url = form.attr('action');
        let type = form.attr('method');

        $.ajax({
            url: url,
            data: data,
            type: type,
            dataType: 'JSON',
            beforeSend: function () {
                $("#j-loader").addClass("lds-roller");
            },
            success: function (login) {
                if (login.status !== 'error') {
                    $(location).attr('href', './');
                    $("#j-loader").removeClass("lds-roller");
                } else {
                    $(".j-error").appendTo('<p>' + login.msg + '</p>');
                    $("#j-loader").removeClass("lds-roller");
                }
            }
        });
        return false;
    }
});
