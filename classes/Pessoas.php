﻿<?php

class Pessoas {
    
    public $conn;
    
    public function __construct ()
    {
        $dsn = 'mysql:host=localhost;dbname=sobreaviso';
        $this->conn = new PDO($dsn, 'root', '');
        $this->conn->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
    }
    
    public function Cadastro ($nome, $username, $password, $salario)
    {
        
        if ($this->getCountPessoasByUserName($username) > 0) {
            return $this->ajaxFunc('error', 'Já existe um usuário com esse username.');
        } else {
            $sql = "INSERT INTO `pessoas` (`nome`, `username`, `password`, `salario`) VALUES (:nome, :username, sha1(:pass), :salario);";
            $result = $this->conn->prepare($sql);
            if ($result->execute(['nome' => $nome, 'username' => $username, 'pass' => $password, 'salario' => $salario])) {
                $pessoa = $this->getPessoasByUsername($username);
                foreach ($pessoa as $key=>$value){
                    $_SESSION[$key] = $value;
                }
                
                return $this->ajaxFunc('success', 'Cadastro realizado com sucesso.');
            } else {
                $_SESSION['username'] = '';
                
                return $this->ajaxFunc('error', 'Houve um erro ao cadastrar.');
            }
        }
    }
    
    public function getCountPessoasByUserName ($username)
    {
        $sql = "SELECT * FROM pessoas where username = :username limit 1";
        $result = $this->conn->prepare($sql);
        $result->execute(['username' => $username]);
        
        return $result->rowCount();
    }
    
    private function ajaxFunc ($ststus, $msg)
    {
        $helper = [
            'status' => $ststus,
            'msg' => $msg
        ];
        
        return $helper;
    }
    
    public function login ($username, $password)
    {
        $sql = 'SELECT * FROM pessoas where username = :username and password = :password';
        $result = $this->conn->prepare($sql);
        $result->execute(['username' => $username, 'password' => sha1($password)]);
        if ($result->rowCount() > 0) {
            $data = $result->fetch();
            $username = $data['username'];
            $pessoa = $this->getPessoasByUsername($username);
            foreach ($pessoa as $key=>$value){
                $_SESSION[$key] = $value;
            }
            
            return $this->ajaxFunc('success', 'Login realizado com sucesso.');
        } else {
            return $this->ajaxFunc('error', 'Usuário ou senha Inválidos');
        }
    }
    
    public function getPessoas ()
    {
        $sql = "SELECT * FROM pessoas";
        $result = $this->conn->prepare($sql);
        if ($result->execute()) {
            return $result->fetchAll();
        } else {
            return $this->ajaxFunc('danger', 'Nenhum resultado encontrado.');
        }
    }
    
    public function getPessoasByName ($name)
    {
        $sql = "SELECT * FROM pessoas where nome like :name";
        $result = $this->conn->prepare($sql);
        $result->execute(['name' => $name]);
        if ($result->rowCount() > 0) {
            return $result->fetch();
        } else {
            return $this->ajaxFunc('danger', 'Nenhum resultado encontrado.');
        }
    }
    
    public function getPessoasByUsername ($username)
    {
        $sql = "SELECT * FROM pessoas where username = :username limit 1";
        $result = $this->conn->prepare($sql);
        $result->execute(['username' => $username]);
        if ($result->rowCount() > 0) {
            return $result->fetch();
        } else {
            return $this->ajaxFunc('error', 'Não foi encontrado nenhum usuário com esse username');
        }
    }
}
