<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Sobreaviso
 *
 * @author ricardo.filho
 */
class Sobreaviso {

    private $salario;

    public function __construct($salario) {
        $this->salario = $salario;
    }

    public function getSalario() {
        return $this->salario;
    }

    public function getHoraTrabalhada() {
        return $this->getSalario() / 220;
    }

    public function getSobreavisoHora() {
        return $this->getHoraTrabalhada() / 3;
    }

    public function getSobreaviso12Horas() {
        return $this->getSobreavisoHora() * 12;
    }

    public function getSobreaviso24Horas() {
        return $this->getSobreavisoHora() * 24;
    }
}
