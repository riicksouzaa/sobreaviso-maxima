<?php
include_once "session.php";
if (!isset($_SESSION['username']) || $_SESSION['username'] === '') {
} else {
    header("Location: ./");
}

?>
<!DOCTYPE html>
<html>
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" href="assets/img/favicon.png" type="image/png">


    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet"
          type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap CSS -->
    <!--        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">-->
    <!--    <link rel="stylesheet" href="https://unpkg.com/bootstrap-material-design@4.1.1/dist/css/bootstrap-material-design.min.css" integrity="sha384-wXznGJNEXNG1NFsbm0ugrLFMQPWswR3lds2VeinahP8N0zJw9VWSopbjv2x7WCvX" crossorigin="anonymous">-->
    <link rel="stylesheet" href="assets/css/bootstrap.css">
    <link rel="stylesheet" href="assets/css/mdb.min.css">
    <link rel="stylesheet" href="assets/css/loader.min.css">
    <link rel="stylesheet" href="assets/css/main.min.css">
    <link rel="stylesheet" href="assets/css/waves.min.css">
    <link rel="stylesheet" href="assets/css/style.min.css">


    <title>Cálculo de sobreaviso - Máxima Sistemas</title>
</head>
<body class="signup-page">
<div class="signup-box">
    <div class="card">
        <div class="body">
            <form id="cadastro" action="Ajax.php" method="POST">
                <div class="logo text-center">
                    <img src="assets/img/logo_maxima.png" width="50%">
                </div>
                <hr>
                <br>
                <input type="hidden" name="type" value="cadastro" />
                <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">person</i>
                        </span>
                    <div class="form-line">
                        <input type="text" class="form-control" name="nome" placeholder="Seu Nome" id="nome" required>
                    </div>
                </div>
                <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">person_outline</i>
                        </span>
                    <div class="form-line">
                        <input type="text" class="form-control" name="username" placeholder="Seu username" required>
                    </div>
                </div>
                <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">attach_money</i>
                        </span>
                    <div class="form-line">
                        <input type="number" class="form-control" name="salario" placeholder="Seu salário" required>
                    </div>
                </div>
                <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">lock</i>
                        </span>
                    <div class="form-line">
                        <input type="password" class="form-control" name="password" minlength="6"
                               placeholder="Digite sua Senha" required>
                    </div>
                </div>
                <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">lock</i>
                        </span>
                    <div class="form-line">
                        <input type="password" class="form-control" name="confirm" minlength="6"
                               placeholder="Confirme a senha" required>
                    </div>
                </div>
                <div class="j-error"></div>
                <div id="j-loader" class="">
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                </div>
                <button class="btn btn-block btn-lg bg-blue waves-effect" type="submit">Cadastre-se</button>
                <div class="m-t-25 m-b--5 align-center">
                    <a href="./login.php">Já possui uma conta?</a>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="copyright text-center">
    <p class="white-text">Desenvolvido por <a class="white-text" href="http://codenome.com">Ricardo Souza</a></p>
</div>

<!-- Jquery Core Js -->
<script src="assets/js/jquery-3.3.1.min.js"></script>

<!-- Bootstrap Core Js -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
        integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
        integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
        crossorigin="anonymous"></script>

<!-- Waves Effect Plugin Js -->
<script src="assets/js/waves.js"></script>

<!-- Validation Plugin Js -->
<script src="assets/js/jquery.validate.js"></script>


<!-- Custom Js -->
<script src="assets/js/admin.js"></script>
<!-- Script para cadastrar -->
<script src="assets/js/cadastro.js"></script>
</body>
</html>