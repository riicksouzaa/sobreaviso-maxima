<?php include_once "session.php";
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

require_once './classes/Sobreaviso.php';
require_once './classes/Pessoas.php';

function Real ($n)
{
    return number_format($n, '2', ',', '.');
}

if(!isset($_REQUEST['type']) || $_REQUEST['type'] === ''){
    $q['status'] = 'error';
    $q['msg'] = 'Ocorreu um erro ao logar-se.';
    echo json_encode($q);
    die();
}else{
    
    
    if ($_REQUEST['type'] == 'cadastro') {
        
        if (isset($_POST)) {
            $dados = $_POST;
            $pessoa = new Pessoas();
            $cadastro = $pessoa->Cadastro($dados['nome'], $dados['username'], $dados['password'], $dados['salario']);
            echo json_encode($cadastro);
            die();
        } else {
            $q['status'] = 'error';
            $q['msg'] = 'Ocorreu um erro ao cadastrar.';
            echo json_encode($q);
            die();
        }
        
    }
    if ($_REQUEST['type'] == 'login') {
        if (isset($_POST)) {
            $dados = $_POST;
            $pessoa = new Pessoas();
            $login = $pessoa->login($dados['username'], $dados['password']);
            echo json_encode($login);
            die();
        } else {
            $q['status'] = 'error';
            $q['msg'] = 'Ocorreu um erro ao logar-se.';
            echo json_encode($q);
            die();
        }
    }
    if ($_REQUEST['type'] == 'sobreaviso') {
        $pessoas = new Pessoas();
        $pessoa = $pessoas->getPessoasByUserName($_SESSION['username']);
        if (!empty($pessoa['status']) && $pessoa['status'] === 'error') {
            echo json_encode($pessoa);
        } else {
            $sobreaviso = new Sobreaviso($pessoa['salario']);
            $q['salario'] = Real($sobreaviso->getSalario());
            $q['horatrabalhada'] = Real($sobreaviso->getHoraTrabalhada());
            $q['horasobreaviso'] = Real($sobreaviso->getSobreavisoHora());
            $q['middaysobreaviso'] = Real($sobreaviso->getSobreaviso12Horas());
            $q['alldaysobreaviso'] = Real($sobreaviso->getSobreaviso24Horas());
            echo json_encode($q);
            die();
        }
    }
}