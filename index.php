<?php include_once "session.php";
require_once "classes/Pessoas.php";
require_once "classes/Datas.php";
if (!isset($_SESSION['username']) || $_SESSION['username'] === '') {
    header("Location: ./login.php");
} else {
    $pessoa = new Pessoas();
    $pe = $pessoa->getPessoasByUsername($_SESSION['username']);
    //    hora_entrada
    //    hora_saida
    //    hora_almoco_inicio
    //    hora_almoco_fim
    //    $entrada = strtotime($pe['hora_entrada']);
    //    $saida = strtotime($pe['hora_saida']);
    //    $almoco_entrada = strtotime($pe['hora_almoco_inicio']);
    //    $almoco_saida = strtotime($pe['hora_almoco_fim']);
    
    //    $DataInicial = "01/09/2018";
    //    $DataFinal = "30/09/2018";
    //    $diasUteis = new Datas();
    //    $diasUteis = $diasUteis->DiasUteis($DataInicial, $DataFinal);
    //    $horas_trabalhadas = ($almoco_entrada - $entrada) / 3600 + ($saida - $almoco_saida) / 3600;
    
    //    var_dump($horas_trabalhadas / 60 / 60);
    if (!empty($pe['status']) && $pe['status'] === 'error') {
        header("Location: ./logout.php");
    }
}
?>

<!doctype html>
<html lang="pt-br">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" href="assets/img/favicon.png" type="image/png">

    <link rel="stylesheet" href="assets/css/bootstrap.css">
    <link rel="stylesheet" href="assets/css/mdb.min.css">
    <link rel="stylesheet" href="assets/css/loader.min.css">
    <link rel="stylesheet" href="assets/css/main.min.css">
    <link rel="stylesheet" href="assets/css/waves.min.css">
    <link rel="stylesheet" href="assets/css/style.min.css">

    <title>Cálculo de sobreaviso - Máxima Sistemas</title>
</head>
<body class="signup-page">
<div class="signup-box">
    <div class="card">
        <div class="body text-center">
            <img src="assets/img/logo_maxima.png" class="logo-maxima" alt="logo_maxima" />
            <hr />
            <h3 class=" text-maxima-blue">Bem vindo, <?= utf8_encode($pe['nome']) ?> <br> Verifique seu Sobreaviso
            </h3>
            <br>
            <div id="j-loader">
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
            </div>
            <p id="j-sobreaviso"></p>
            <a href="logout.php" class="white-text">
                <button class="btn btn-primary btn-block"> Sair</button>
            </a>
        </div>
    </div>
</div>
<div class="copyright text-center">
    <p class="white-text">Desenvolvido por <a class="white-text" href="http://codenome.com">Ricardo Souza</a></p>
</div>
<!-- Jquery Core Js -->
<script src="assets/js/jquery-3.3.1.min.js"></script>

<!-- Bootstrap Core Js -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
        integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
        integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
        crossorigin="anonymous"></script>

<!-- Waves Effect Plugin Js -->
<script src="assets/js/waves.js"></script>

<!-- Validation Plugin Js -->
<script src="assets/js/jquery.validate.js"></script>


<!-- Custom Js -->
<script src="assets/js/admin.js"></script>
<script src="assets/js/main.js"></script>
</body>
</html>