CREATE TABLE `sobreaviso`.`pessoas` (
  `id`                 INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `nome`               VARCHAR(255) NOT NULL,
  `username`           VARCHAR(255) NOT NULL,
  `password`           varchar(255) not null,
  `salario`            FLOAT        NOT NULL,
  `hora_entrada`       TIME         NOT NULL DEFAULT '08:00:00',
  `hora_saida`         TIME         NOT NULL DEFAULT '18:00:00',
  `hora_almoco_inicio` TIME         NOT NULL DEFAULT '12:00:00',
  `hora_almoco_fim`    TIME         NOT NULL DEFAULT '13:12:00',
  `dta_cadastro`       DATETIME     NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `dta_update`         DATETIME     NOT NULL DEFAULT CURRENT_TIMESTAMP
  ON UPDATE CURRENT_TIMESTAMP,
  `dta_exclusao`       DATETIME     NULL,
  `is_active`          TINYINT      NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `username_UNIQUE` (`username` ASC)
);
